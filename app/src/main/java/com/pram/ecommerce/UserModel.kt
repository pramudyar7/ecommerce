package com.pram.ecommerce

data class UserModel(
    var access_token: String,
    var refresh_token: String,
    var name: String,
    var isLogin: Boolean
)