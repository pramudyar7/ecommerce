package com.pram.ecommerce

data class OnboardingModel(
    val isOnboardingComplete: Boolean
)
