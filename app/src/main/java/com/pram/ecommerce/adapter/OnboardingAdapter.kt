package com.pram.ecommerce.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.pram.ecommerce.OnboardingItem
import com.pram.ecommerce.databinding.OnboardingItemContainerBinding

class OnboardingAdapter(private val onboardingItem: List<OnboardingItem>) :
    RecyclerView.Adapter<OnboardingAdapter.ViewHolder>() {
    inner class ViewHolder(private var binding: OnboardingItemContainerBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun binding(onboardingItem: OnboardingItem) {
            with(binding) {
                Glide.with(itemView)
                    .load(onboardingItem.image)
                    .into(imgOnboarding)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = OnboardingItemContainerBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {

        return onboardingItem.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding(onboardingItem[position])
    }
}