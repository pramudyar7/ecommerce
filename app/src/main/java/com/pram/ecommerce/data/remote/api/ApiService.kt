package com.pram.ecommerce.data.remote.api

import com.pram.ecommerce.LoginInformation
import com.pram.ecommerce.data.remote.response.AuthResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface ApiService {

    @POST("login")
    @Headers(apiKey)
    fun login(
        @Body loginInformation: LoginInformation
    ): Call<AuthResponse>

    @Headers("API_KEY:6f8856ed-9189-488f-9011-0ff4b6c08edc")
    @POST("register")
    fun register(
        @Body loginInformation: LoginInformation
    ): Call<AuthResponse>

    companion object {
        const val apiKey = "API_KEY:6f8856ed-9189-488f-9011-0ff4b6c08edc"
    }
}