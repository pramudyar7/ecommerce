package com.pram.ecommerce.ui

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.pram.ecommerce.R
import com.pram.ecommerce.databinding.ActivityProfileBinding

class ProfileActivity : AppCompatActivity() {
    private lateinit var binding: ActivityProfileBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.apply {
            val name = etName.text.toString()
            btnRegister.setOnClickListener {
                startActivity(Intent(this@ProfileActivity, MainActivity::class.java))
            }
            Glide.with(this@ProfileActivity)
                .load(R.drawable.baseline_person_24)
                .into(imgProfile)
            imgProfile.setOnClickListener {
                showDialog()
            }
        }
    }

    private fun showDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_choose_profile_picture)
        dialog.show()
        dialog.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
        )
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window?.setGravity(Gravity.CENTER)
        dialog.findViewById<TextView>(R.id.tvCamera)?.setOnClickListener {

        }
        dialog.findViewById<TextView>(R.id.tvCancel)?.setOnClickListener {
            dialog.cancel()
        }
    }
}