package com.pram.ecommerce.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.pram.ecommerce.OnboardingItem
import com.pram.ecommerce.R
import com.pram.ecommerce.UserPreferences
import com.pram.ecommerce.adapter.OnboardingAdapter
import com.pram.ecommerce.databinding.ActivityOnboardingBinding
import com.pram.ecommerce.viewmodel.OnboardingViewModel
import com.pram.ecommerce.viewmodel.ViewModelFactory

val Context.dataStore: DataStore<Preferences> by preferencesDataStore("settings")

class OnboardingActivity : AppCompatActivity() {
    private lateinit var binding: ActivityOnboardingBinding
    private lateinit var viewModel: OnboardingViewModel
    private lateinit var adapter: OnboardingAdapter
    private lateinit var indicatorContainer: LinearLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        installSplashScreen()
        super.onCreate(savedInstanceState)

        binding = ActivityOnboardingBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.hide()
        setupOnboardingItem()
        setupIndicator()
        setCurrentIndicator(0)
        binding.apply {
            btnJoinNow.setOnClickListener {
                startActivity(Intent(this@OnboardingActivity, RegisterActivity::class.java))
                viewModel.saveOnboardingStatus()
            }
        }
        viewModel = ViewModelProvider(
            this,
            ViewModelFactory(UserPreferences.getInstance(dataStore))
        )[OnboardingViewModel::class.java]
        viewModel.getStatus().observe(this) {
            if (it.isOnboardingComplete) {
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            }
        }
        viewModel.getUser().observe(this) {
            if (it.isLogin) {
                if (it.name.isEmpty()) {
                    startActivity(Intent(this, ProfileActivity::class.java))
                    finish()
                } else {
                    startActivity(Intent(this, MainActivity::class.java))
                    finish()
                }
            }
        }
    }

    private fun setupOnboardingItem() {
        adapter = OnboardingAdapter(
            listOf(
                OnboardingItem(R.drawable.ic_android_black_24dp),
                OnboardingItem(R.drawable.ic_android_black_24dp),
                OnboardingItem(R.drawable.ic_android_black_24dp),
            )
        )
        binding.apply {
            viewPagerOnboarding.adapter = adapter
            viewPagerOnboarding.registerOnPageChangeCallback(object :
                OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    setCurrentIndicator(position)
                    if (position == adapter.itemCount.minus(1)) {
                        tvNext.visibility = View.GONE
                    } else {
                        tvNext.visibility = View.VISIBLE
                    }

                }
            })
//            (viewPagerOnboarding.getChildAt(0) as RecyclerView).overScrollMode =
//                RecyclerView.OVER_SCROLL_NEVER
            tvNext.setOnClickListener {
                if (viewPagerOnboarding.currentItem + 1 < adapter.itemCount) {
                    viewPagerOnboarding.currentItem += 1
                }
            }
            tvSkip.setOnClickListener {
                startActivity(Intent(this@OnboardingActivity, LoginActivity::class.java))
                viewModel.saveOnboardingStatus()
            }
        }
    }

    private fun setupIndicator() {
        indicatorContainer = binding.dotIndicator
        val indicators = arrayOfNulls<ImageView>(adapter.itemCount)
        val layoutParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        layoutParams.setMargins(32, 0, 32, 0)
        for (i in indicators.indices) {
            indicators[i] = ImageView(applicationContext)
            indicators[i]?.let {
                it.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.onboarding_indicator_inactive
                    )
                )
                it.layoutParams = layoutParams
                indicatorContainer.addView(it)
            }
        }
    }

    private fun setCurrentIndicator(position: Int) {
        val childCount = indicatorContainer.childCount
        for (i in 0 until childCount) {
            val imageView = indicatorContainer.getChildAt(i) as ImageView
            if (i == position) {
                imageView.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext, R.drawable.onboarding_indicator_active
                    )
                )
            } else {
                imageView.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext, R.drawable.onboarding_indicator_inactive
                    )
                )
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.apply {
//            viewPagerOnboarding.unregisterOnPageChangeCallback(object : OnPageChangeCallback {})
        }
    }
}