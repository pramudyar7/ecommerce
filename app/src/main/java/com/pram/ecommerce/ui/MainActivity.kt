package com.pram.ecommerce.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.pram.ecommerce.R
import com.pram.ecommerce.databinding.ActivityMainBinding
import com.pram.ecommerce.fragment.HomeFragment
import com.pram.ecommerce.fragment.HomeFragment2
import com.pram.ecommerce.fragment.HomeFragment3

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.apply {
            botNav.setOnItemSelectedListener {
                when (it.itemId) {
                    R.id.homeFragment -> replaceFragment(HomeFragment())

                    R.id.historyFragment -> replaceFragment(HomeFragment2())

                    R.id.profileFragment -> replaceFragment(HomeFragment3())

                    else -> {}
                }
                true
            }
        }
    }

    fun replaceFragment(fragment: Fragment) {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragmentContainer, fragment)
        fragmentTransaction.commit()
    }
}