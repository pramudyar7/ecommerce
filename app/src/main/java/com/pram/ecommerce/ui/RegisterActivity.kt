package com.pram.ecommerce.ui

import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.pram.ecommerce.UserModel
import com.pram.ecommerce.UserPreferences
import com.pram.ecommerce.databinding.ActivityRegisterBinding
import com.pram.ecommerce.isValidEmail
import com.pram.ecommerce.viewmodel.AuthViewModel
import com.pram.ecommerce.viewmodel.ViewModelFactory

class RegisterActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRegisterBinding
    private lateinit var viewModel: AuthViewModel
    private lateinit var userModel: UserModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel = ViewModelProvider(
            this,
            ViewModelFactory(UserPreferences.getInstance(dataStore))
        )[AuthViewModel::class.java]
        viewModel.isLoading.observe(this) {
            showLoading(it)
        }
        viewModel.getUser().observe(this) {
            userModel = UserModel(
                it.access_token,
                it.refresh_token,
                it.name,
                it.isLogin
            )
            if (it.isLogin) {
                startActivity(Intent(this@RegisterActivity, ProfileActivity::class.java))
                finish()
            }
        }
        binding.apply {

            btnRegister.setOnClickListener {
                val email = etEmail.text.toString()
                val password = etPassword.text.toString()

                tvExampleEmail.visibility = View.GONE
                tvExamplePassword.visibility = View.GONE
                layoutEmail.error = null
                layoutPassword.error = null
                layoutEmail.setErrorIconTintMode(PorterDuff.Mode.CLEAR)
                layoutPassword.setErrorIconTintMode(PorterDuff.Mode.CLEAR)
                if (email.isEmpty() || password.isEmpty()) {
                    if (email.isEmpty()) {
                        layoutEmail.error = "Email Tidak boleh Kosong"
                    }
                    if (password.isEmpty()) {
                        layoutPassword.error = "Password Tidak Boleh Kosong"
                    }
                } else if (!email.isValidEmail() || password.length < 8) {
                    if (!email.isValidEmail()) {
                        layoutEmail.error = "Email Tidak Valid"
                    }
                    if (password.length < 8) {
                        layoutPassword.error = "Minimal 8 Karakter"
                    }

                } else {
                    viewModel.register(email, password)
                    isSuccess()
                }
            }
            btnMoveToLogin.setOnClickListener {
                startActivity(Intent(this@RegisterActivity, LoginActivity::class.java))
            }
        }
    }

    private fun showLoading(isLoading: Boolean) {
        if (isLoading) {
            binding.progressBar.visibility = View.VISIBLE
        } else {
            binding.progressBar.visibility = View.GONE
        }
    }

    private fun isSuccess() {
        if (userModel.isLogin) {
            startActivity(Intent(this@RegisterActivity, ProfileActivity::class.java))
            finish()
        }
    }
}