package com.pram.ecommerce.ui

import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.ViewModelProvider
import com.pram.ecommerce.UserModel
import com.pram.ecommerce.UserPreferences
import com.pram.ecommerce.databinding.ActivityLoginBinding
import com.pram.ecommerce.isValidEmail
import com.pram.ecommerce.viewmodel.AuthViewModel
import com.pram.ecommerce.viewmodel.ViewModelFactory

class LoginActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding
    private lateinit var viewModel: AuthViewModel
    private lateinit var userModel: UserModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel = ViewModelProvider(
            this,
            ViewModelFactory(UserPreferences.getInstance(dataStore))
        )[AuthViewModel::class.java]
        viewModel.getUser().observe(this) {
            userModel = UserModel(
                it.access_token,
                it.refresh_token,
                it.name,
                it.isLogin
            )
            if (it.isLogin) {
                startActivity(Intent(this@LoginActivity, ProfileActivity::class.java))
                finish()
            }
        }
        viewModel.isLoading.observe(this) {
            showLoading(it)
        }
        viewModel.isValidEmail.observe(this) { isValid ->
            if (!isValid) {
                binding.apply {
                    layoutEmail.error = "Error"
                }
            }
            binding.btnLogin.isEnabled = isValid
        }
        binding.apply {
            etEmail.addTextChangedListener {
                object : TextWatcher {
                    override fun beforeTextChanged(
                        s: CharSequence?,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {
                        TODO("Not yet implemented")
                    }

                    override fun onTextChanged(
                        s: CharSequence?,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {
                        viewModel.validateInput(s.toString())
                    }

                    override fun afterTextChanged(s: Editable?) {
                        viewModel.inputText.value = s.toString()
                    }
                }
            }
            btnMoveToRegister.setOnClickListener {
                startActivity(Intent(this@LoginActivity, RegisterActivity::class.java))
            }

            btnLogin.setOnClickListener {
                val email = etEmail.text.toString()
                val password = etPassword.text.toString()
                layoutEmail.error = null
                layoutPassword.error = null
                layoutEmail.setErrorIconTintMode(PorterDuff.Mode.CLEAR)
                layoutPassword.setErrorIconTintMode(PorterDuff.Mode.CLEAR)
                if (email.isEmpty() || password.isEmpty()) {
                    if (email.isEmpty()) {
                        layoutEmail.error = "Email Tidak boleh Kosong"
                    }
                    if (password.isEmpty()) {
                        layoutPassword.error = "Password Tidak Boleh Kosong"
                    }
                } else if (!email.isValidEmail() || password.length < 8) {
                    if (!email.isValidEmail()) {
                        layoutEmail.error = "Email Tidak Valid"
                    }
                    if (password.length < 8) {
                        layoutPassword.error = "Minimal 8 Karakter"
                    }

                } else {
                    viewModel.login(email, password)
                    isSuccess()
                }
            }
        }
    }

    private fun isSuccess() {
        if (userModel.isLogin) {
            startActivity(Intent(this@LoginActivity, ProfileActivity::class.java))
            finish()
        }
    }

    private fun showLoading(isLoading: Boolean) {
        binding.progressBar.visibility = if (isLoading) View.VISIBLE else View.GONE
    }
}