package com.pram.ecommerce

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class UserPreferences private constructor(private val dataStore: DataStore<Preferences>) {
    suspend fun saveUser(userModel: UserModel) {
        dataStore.edit {
            it[ACCESS_TOKEN] = userModel.access_token
            it[REFRESH_TOKEN] = userModel.refresh_token
            it[NAME] = userModel.name
            it[LOGIN_STATE] = userModel.isLogin
        }
    }

    fun getUser(): Flow<UserModel> {
        return dataStore.data.map {
            UserModel(
                it[ACCESS_TOKEN] ?: "",
                it[REFRESH_TOKEN] ?: "",
                it[NAME] ?: "",
                it[LOGIN_STATE] ?: false

            )
        }
    }

    suspend fun saveStatus(onBoardingModel: OnboardingModel) {
        dataStore.edit {
            it[ONBOARDING_STATUS] = true
        }
    }

    fun getStatus(): Flow<OnboardingModel> {
        return dataStore.data.map {
            OnboardingModel(
                it[ONBOARDING_STATUS] ?: false
            )
        }
    }

    companion object {
        @Volatile
        private var INSTANCE: UserPreferences? = null
        private val ONBOARDING_STATUS = booleanPreferencesKey("status")
        private val ACCESS_TOKEN = stringPreferencesKey("access_token")
        private val REFRESH_TOKEN = stringPreferencesKey("refresh_token")
        private val NAME = stringPreferencesKey("name")
        private val LOGIN_STATE = booleanPreferencesKey("loading_state")
        fun getInstance(dataStore: DataStore<Preferences>): UserPreferences {
            return INSTANCE ?: synchronized(this) {
                val instance = UserPreferences(dataStore)
                INSTANCE = instance
                instance
            }
        }
    }
}