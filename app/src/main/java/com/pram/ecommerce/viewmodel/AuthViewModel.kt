package com.pram.ecommerce.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.pram.ecommerce.LoginInformation
import com.pram.ecommerce.UserModel
import com.pram.ecommerce.UserPreferences
import com.pram.ecommerce.data.remote.api.ApiConfig
import com.pram.ecommerce.data.remote.response.AuthResponse
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AuthViewModel(private val pref: UserPreferences) : ViewModel() {
    private val _isValidEmail = MutableLiveData<Boolean>()
    val isValidEmail: LiveData<Boolean> = _isValidEmail
    private val _isValidPassword = MutableLiveData<Boolean>()
    val isValidPassword: LiveData<Boolean> = _isValidPassword
    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    val inputText = MutableLiveData<String>()
    val isInputValid: LiveData<Boolean> = Transformations.map(inputText) { text ->
        text.isNotEmpty() && text.length >= 5
    }

    init {

    }

    fun validateInput(text: String) {
        _isValidEmail.value = android.util.Patterns.EMAIL_ADDRESS.matcher(text).matches()
    }

    fun login(email: String, password: String) {
        val userInfo = LoginInformation(email, password)
        _isLoading.value = true
        val client = ApiConfig.getApiService().login(userInfo)
        client.enqueue(object : Callback<AuthResponse> {
            override fun onResponse(call: Call<AuthResponse>, response: Response<AuthResponse>) {
                val responseBody = response.body()
                if (response.isSuccessful) {
                    if (responseBody != null) {
                        _isLoading.value = false
                        val userData = UserModel(
                            access_token = responseBody.data.accessToken,
                            refresh_token = responseBody.data.refreshToken,
                            name = "",
                            isLogin = true
                        )
                        saveUser(userData)
                    }
                }
            }

            override fun onFailure(call: Call<AuthResponse>, t: Throwable) {
                _isLoading.value = false
            }
        })
    }

    fun register(email: String, password: String) {
        val userInfo = LoginInformation(email, password)
        _isLoading.value = true
        val client = ApiConfig.getApiService().register(userInfo)
        client.enqueue(object : Callback<AuthResponse> {
            override fun onResponse(call: Call<AuthResponse>, response: Response<AuthResponse>) {
                val responseBody = response.body()
                if (response.isSuccessful) {
                    if (responseBody != null) {
                        _isLoading.value = false
                        val userData = UserModel(
                            access_token = responseBody.data.accessToken,
                            refresh_token = responseBody.data.refreshToken,
                            name = "",
                            isLogin = true
                        )
                        saveUser(userData)
                    }
                }
            }

            override fun onFailure(call: Call<AuthResponse>, t: Throwable) {
                _isLoading.value = false
            }
        })
    }

    fun saveUser(user: UserModel) {
        viewModelScope.launch {
            pref.saveUser(user)
        }
    }

    fun getUser(): LiveData<UserModel> {
        return pref.getUser().asLiveData()
    }
}