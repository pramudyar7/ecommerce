package com.pram.ecommerce.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.pram.ecommerce.OnboardingModel
import com.pram.ecommerce.UserModel
import com.pram.ecommerce.UserPreferences
import kotlinx.coroutines.launch

class OnboardingViewModel(private val pref: UserPreferences) : ViewModel() {

    fun getStatus(): LiveData<OnboardingModel> {
        return pref.getStatus().asLiveData()
    }

    fun saveOnboardingStatus() {
        val onboardingModel = OnboardingModel(
            true
        )
        saveUser(onboardingModel)
    }

    private fun saveUser(onBoardingStatus: OnboardingModel) {
        viewModelScope.launch {
            pref.saveStatus(onBoardingStatus)
        }
    }

    fun getUser(): LiveData<UserModel> {
        return pref.getUser().asLiveData()
    }
}