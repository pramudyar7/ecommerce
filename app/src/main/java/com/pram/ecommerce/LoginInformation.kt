package com.pram.ecommerce

data class LoginInformation(
    var email: String,
    var password: String
)
